from rest_framework import serializers
from .models import Weather


class WeatherSerializer(serializers.ModelSerializer):
    year_month = serializers.SerializerMethodField()

    class Meta:
        model = Weather
        fields = ('value', 'year_month')

    def get_year_month(self, obj):
        return '{}-{}'.format(obj.year, obj.month)
