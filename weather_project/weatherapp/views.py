from django.shortcuts import render
from django.http import HttpResponse
import urllib.request, json
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Weather, WeatherParams
from .serializers import WeatherSerializer
from django.db.models import Q
# Create your views here.


class WeatherList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request):
        if self.request.query_params.get('start_date') and \
                self.request.query_params.get('end_date') and \
                self.request.query_params.get('metric_type') and \
                self.request.query_params.get('location'):
            start_date = self.request.query_params.get('start_date')
            start_date = start_date.split('-')
            start_year = start_date[0]
            end_date = self.request.query_params.get('end_date')
            end_date = end_date.split('-')
            end_year = end_date[0]
            metric_type = self.request.query_params.get('metric_type')
            location = self.request.query_params.get('location')
            weather_params_data = WeatherParams.objects.get(metric_type=metric_type,
                                                            location=location)
            weather_data = Weather.objects.filter(Q(year__range=(int(start_year),
                                                                 int(end_year))) &
                                                  Q(weather_params=weather_params_data))
            serializer_data = WeatherSerializer(weather_data, many=True)
            return Response(serializer_data.data, status=status.HTTP_200_OK)
        else:
            return Response("Bad Request", status=status.HTTP_400_BAD_REQUEST)


def fetch_json_data(request):
    """
    Function to fetch weather json data and store it into model
    :param request:
    :return:
    """
    if request.method == "POST":
        metric = request.POST.get('Metric')
        location = request.POST.get('Location')
        try:
            json_url = 'https://s3.eu-west-2.amazonaws.com/interview-question-data/metoffice/' + \
                       metric + "-" + location + ".json"
            weather_param_obj = WeatherParams.objects.create(metric_type=metric, location=location)
            with urllib.request.urlopen(json_url) as url:
                response_data = json.loads(url.read().decode())
            for res in response_data:
                weather_obj = Weather.objects.create(weather_params=weather_param_obj,
                                                     value=res['value'], month=res['month'],
                                                     year=res['year'])
        except:
            return HttpResponse('URL not Found!!')
        return HttpResponse('Data fetched successfully!!')
    else:
        return render(request, 'read-json.html')
