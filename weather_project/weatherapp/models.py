from django.db import models

# Create your models here.
class WeatherParams(models.Model):
    metric_type = models.CharField(blank=False, max_length=100, unique=True)
    location = models.CharField(blank=False, max_length=100, unique=True)

class Weather(models.Model):
    weather_params = models.ForeignKey(WeatherParams, on_delete=models.CASCADE)
    value = models.FloatField(default=0.0)
    month = models.IntegerField()
    year = models.IntegerField()
