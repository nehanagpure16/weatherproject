from django.urls import path
from . import views

urlpatterns = [
    path('fetch/data/', views.fetch_json_data, name='fetch_json_data'),
    path('weather/', views.WeatherList.as_view()),
]