# WeatherProject

Rest Apis for weather data using Django and DRF.

URL to fetch JSON data:
http://localhost:8000/fetch/data/
Please provide valid Metric and Location to fetch JSON data.

URL to GET Weather JSON data API:
http://localhost:8000/weather/?start_date=2015-01&end_date=2019-01&metric_type=Rainfall&location=England

Require Query Parameters:
start_date
end_date
metric_type
location
